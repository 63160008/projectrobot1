/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.robotproject1;

import java.util.Scanner;

/**
 *
 * @author nonta
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TableMap map = new TableMap(10,10);
        robot robot = new robot(2,2,'x',map);
        bomb bomb = new bomb (5,5);
        map.setRobot(robot);
        map.setBomb(bomb);
        map.showMap();
        while(true){
            map.showMap();
            char direction = inputDirection(sc);
            if(direction=='q'){
                printbyebye();
                break;
            }
            robot.walk(direction);
        }
    }

    private static void printbyebye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner sc) {
        String str = sc.next();
        char direction = str.charAt(0);
        return direction;
    }
}
