/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.robotproject1;

/**
 *
 * @author nonta
 */
public class bomb {

    private int x;
    private int y;
    private char symbol;

    public bomb(int x, int y) {
        this.x = x;
        this.y = y;
        this.symbol = 'b';
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }
     public boolean isOn(int x,int y){
        return this.x == x && this.y == y;
    }
}
